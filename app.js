var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var neo4j = require('neo4j-driver').v1;

var app = express();
var http = require('http').createServer(app);
//var io = require('socket.io').listen(http);

//View engine 
app.set('views', path.join(__dirname, 'views'));
app.set('view engine' , 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use(express.static(path.join(__dirname,'public')));

//connect to neo4j
var driver = neo4j.driver('bolt://localhost',neo4j.auth.basic('neo4j','admin'));
var session = driver.session();


//setup the routes
app.get('/', function(req,res){
    res.render('index' );
});


/// brokers_bss 
app.get('/brokers_bss', function(req,res){
    session 
        .run('MATCH (b:Broker) RETURN b.Description, b.SourceSystem;')
        .then(function(result){
            result.records.forEach(function(record){
                var recordArr = [];
                console.log("broker: " + record._fields[0]);
                console.log("source system: "+ record._fields[1]);
             
                recordArr.push({
                    broker: record._fields[0],
                    sourceSystem: record._fields[1]
                });

                recordArr.forEach(function(r){
                    console.log(r.description);   
                });
                //render if need to 
                res.render('brokers_bss' );
            
            })
        })
        .catch(function(err){
            console.log(err);
        });
    

});
/// brokers_bss_products 
app.get('/brokers_bss_products', function(req,res){
    session 
        .run('MATCH (b:Broker)-[:defined_by_broker]-(bt:BrokerTemplate)-[:defined_by_broker]-(pt:ProductTemplate) RETURN b.SourceSystem as SourceSystem, b.Description as Broker, pt.Description as Product ORDER BY SourceSystem, Broker, Product;')
        .then(function(result){
            result.records.forEach(function(record){
                var recordArr = [];
                console.log("broker: " + record._fields[0]);
                console.log("source system: "+ record._fields[1]);
                console.log("product: "+ record._fields[2]);
             
                recordArr.push({
                    broker: record._fields[0],
                    sourceSystem: record._fields[1],
                    product: record._fields[2]
                });

                recordArr.forEach(function(r){
                    console.log(r.description);   
                });
                //render if need to 
                res.render('brokers_bss_products' );
            
            })
        })
        .catch(function(err){
            console.log(err);
        });
   

});
// brokers_bss_products_sections
app.get('/brokers_bss_products_sections', function(req,res){
    session 
        
        .run('MATCH (b:Broker)-[:defined_by_broker]-(bt:BrokerTemplate)-[:defined_by_broker]-(pt:ProductTemplate)-[:belongs_to_product_template]-(pts:ProductTemplateSection) RETURN DISTINCT b.SourceSystem as SourceSystem, b.Description as Broker, pt.Description as Product, pts.Description as Section ORDER BY SourceSystem, Broker, Product, Section;')
        .then(function(result){
            result.records.forEach(function(record){
                var recordArr = [];
                console.log("broker: " + record._fields[0]);
                console.log("source system: "+ record._fields[1]);
                console.log("product: "+ record._fields[2]);
                console.log("section: "+ record._fields[3]);

                recordArr.push({
                    broker: record._fields[0],
                    sourceSystem: record._fields[1],
                    product: record._fields[2],
                    section: record._fields[3]
                });

                recordArr.forEach(function(r){
                    //console.log(r.description);   
                });
                //render if need to 
                res.render('brokers_bss_products_sections' );
            
            })
        })
        .catch(function(err){
            console.log(err);
        });
   

});
// brokers_bss_products_sections_subsections
app.get('/brokers_bss_products_sections_subsections', function(req,res){
    session 
        .run('MATCH (b:Broker)-[:defined_by_broker]-(bt:BrokerTemplate)-[:defined_by_broker]-(pt:ProductTemplate)-[:belongs_to_product_template]-(pts:ProductTemplateSection)-[:belongs_to_product_template]-(ptss:ProductTemplateSubSection) RETURN DISTINCT b.SourceSystem as SourceSystem, b.Description as Broker, pt.Description as Product, pts.Description as Section, ptss.Description as SubSection ORDER BY SourceSystem, Broker, Product, Section,SubSection ;')
        .then(function(result6){
            result6.records.forEach(function(record){
                var recordArr = [];
                console.log("broker: " + record._fields[0]);
                console.log("source system: "+ record._fields[1]);
                console.log("product: "+ record._fields[2]);
                console.log("section: "+ record._fields[3]);
                console.log("sub section: "+ record._fields[4]);

                recordArr.push({
                    broker: record._fields[0],
                    sourceSystem: record._fields[1],
                    product: record._fields[2],
                    section: record._fields[3],
                    subSection: record._fields[4]
                });

                recordArr.forEach(function(r){
                    //console.log(r.description);   
                });
                //render if need to 
                res.render('brokers_bss_products_sections_subsections' );
            
            })
        })
        .catch(function(err){
            console.log(err);
        });
    

});
//brokers_bss_products_sections_subsections_extensions
app.get('/brokers_bss_products_sections_subsections_extensions', function(req,res){
    session 
        
        .run('MATCH (b:Broker)-[:defined_by_broker]-(bt:BrokerTemplate)-[:defined_by_broker]-(pt:ProductTemplate)-[:belongs_to_product_template]-(pts:ProductTemplateSection)-[:belongs_to_product_template]-(ptss:ProductTemplateSubSection)-[:belongs_to_product_template]-(ptsse:ProductTemplateSubSectionExtension) RETURN DISTINCT b.SourceSystem as SourceSystem, b.Description as Broker, pt.Description as Product, pts.Description as Section, ptss.Description as SubSection, ptsse.Description as SubSectionExtension ORDER BY SourceSystem, Broker, Product, Section,SubSection, SubSectionExtension;')
        .then(function(result7){
            result7.records.forEach(function(record){
                var recordArr = [];
                console.log("/nbroker: " + record._fields[0]);
                console.log("source system: "+ record._fields[1]);
                console.log("product: "+ record._fields[2]);
                console.log("section: "+ record._fields[3]);
                console.log("sub section: "+ record._fields[4]);
                console.log("subsection Extension: "+ record._fields[5]);

                recordArr.push({
                    broker: record._fields[0],
                    sourceSystem: record._fields[1],
                    product: record._fields[2],
                    section: record._fields[3],
                    subSection: record._fields[4],
                    subSectionExtension: record._fields[5]
                });

                recordArr.forEach(function(r){
                    //console.log(r.description);   
                });
                //render if need to 
                res.render('brokers_bss_products_sections_subsections_extensions' );
            
            })
        })
        .catch(function(err){
            console.log(err);
        });
    

});




/// insurer_approved_product 
app.get('/insurer_approved_product', function(req,res){
    session 
     
        .run('MATCH (n:InfinitiProduct) RETURN  n.Description as Product ORDER BY Product;')
        .then(function(result){
            var recordArr = [];
            result.records.forEach(function(record){
                
                console.log("product: "+ record._fields[0]);

                recordArr.push({
                    insurer_product: record._fields[0]
                });

                recordArr.forEach(function(r){
                    console.log(r.description);   
                });
                //render if need to 
                res.render('insurer_approved_product' );
            
            })
            return;
        })
        .catch(function(err){
            console.log(err);
        });
   

});
// insurer_approved_product_section
app.get('/insurer_approved_product_section', function(req,res){
    session 
        
        .run('MATCH (n:InfinitiProduct)-[:belongs_to_infiniti_product]-(is:InfinitiProductSection) RETURN n.Description as Product, is.Description as Section ORDER BY Product, Section;')
        .then(function(result){
            var recordArr = [];
            result.records.forEach(function(record){
               
                console.log("I product: "+ record._fields[0]);
                console.log("I section: "+ record._fields[1]);
                

                recordArr.push({
                    insurer_product: record._fields[0],
                    section: record._fields[1]
                    
                });

                recordArr.forEach(function(r){
                    //console.log(r.description);   
                });
                //render if need to 
                res.render('insurer_approved_product_section' );
            
            })
            return;
        })
        .catch(function(err){
            console.log(err);
        });
    

});
// insurer_approved_product_section_subsection
app.get('/insurer_approved_product_section_subsection', function(req,res){
    session 
        
        .run('MATCH (n:InfinitiProduct)-[:belongs_to_infiniti_product]-(is:InfinitiProductSection)-[:belongs_to_infiniti_product]-(iss:InfinitiProductSubSection) RETURN n.Description as Product, is.Description as Section, iss.Description as SubSection ORDER BY Product, Section, SubSection')
        .then(function(result){

            var recordArr = [];

            result.records.forEach(function(record){
                
                console.log("I product: "+ record._fields[0]);
                console.log("I section: "+ record._fields[1]);
                console.log("I sub section: "+ record._fields[2]);
                

                recordArr.push({
                    insurer_product: record._fields[0],
                    section: record._fields[1],
                    subSection: record._fields[2]
                });

                recordArr.forEach(function(r){
                    //console.log(r.description);   
                });
                //render if need to 
                res.render('insurer_approved_product_section_subsection' );
            
            })
            return;
        })
        .catch(function(err){
            console.log(err);
        });
     

});
//insurer_approved_product_section_subectionextension
app.get('/insurer_approved_product_section_subectionextension', function(req,res){
    session 
        
        .run('MATCH (n:InfinitiProduct)-[:belongs_to_infiniti_product]-(is:InfinitiProductSection)-[:belongs_to_infiniti_product]-(iss:InfinitiProductSubSection)-[:belongs_to_infiniti_product]-(se:InfinitiProductSubSectionExtension) RETURN n.Description as Product, is.Description as Section, iss.Description as SubSection, se.Description as SectionExtension ORDER BY Product, Section, SubSection, SectionExtension')
        .then(function(result){
            var recordArr = [];
            
            result.records.forEach(function(record){
               
                console.log("I product: "+ record._fields[0]);
                console.log("I section: "+ record._fields[1]);
                console.log("I sub section: "+ record._fields[2]);
                console.log("I subsection Extension: "+ record._fields[3]);

                recordArr.push({
                    insurer_product: record._fields[0],
                    section: record._fields[1],
                    subSection: record._fields[2],
                    subSectionExtension: record._fields[3]
                });

                recordArr.forEach(function(r){
                    //console.log(r.description);   
                });
                //render if need to 
                res.render('insurer_approved_product_section_subectionextension' );
            
            })
            return;
        })
        .catch(function(err){
            console.log(err);
        });
   

});

//listen on a port
app.listen(3000);
console.log('Started on port 3000');

module.exports = app;
